<?php
/**
 * @file
 * bintangtimur.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bintangtimur_taxonomy_default_vocabularies() {
  return array(
    'jenis_kegiatan' => array(
      'name' => 'Jenis kegiatan',
      'machine_name' => 'jenis_kegiatan',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'kelas' => array(
      'name' => 'Kelas',
      'machine_name' => 'kelas',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'kurikulum' => array(
      'name' => 'Kurikulum',
      'machine_name' => 'kurikulum',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'sub_kurikulum' => array(
      'name' => 'Sub kurikulum',
      'machine_name' => 'sub_kurikulum',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
