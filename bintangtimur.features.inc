<?php
/**
 * @file
 * bintangtimur.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bintangtimur_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bintangtimur_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function bintangtimur_node_info() {
  $items = array(
    'alumni' => array(
      'name' => t('Alumni'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nama'),
      'help' => '',
    ),
    'artikel' => array(
      'name' => t('Artikel'),
      'base' => 'node_content',
      'description' => t('**TODO** Add some description here'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'berita' => array(
      'name' => t('Berita'),
      'base' => 'node_content',
      'description' => t('**TODO** add some description here'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'fasilitas' => array(
      'name' => t('Fasilitas'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nama Fasilitas'),
      'help' => '',
    ),
    'guru_pegawai' => array(
      'name' => t('Guru / Pegawai'),
      'base' => 'node_content',
      'description' => t('Tambahakan data guru / pegawai'),
      'has_title' => '1',
      'title_label' => t('Nama'),
      'help' => '',
    ),
    'halaman' => array(
      'name' => t('Halaman'),
      'base' => 'node_content',
      'description' => t('**TODO** Add some description here'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'kegiatan' => array(
      'name' => t('Kegiatan'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'kelas' => array(
      'name' => t('Kelas'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nama'),
      'help' => '',
    ),
    'kepala_sekolah' => array(
      'name' => t('Kepala Sekolah'),
      'base' => 'node_content',
      'description' => t('Tambahkan masa jabatan kepala sekolah'),
      'has_title' => '1',
      'title_label' => t('Nama'),
      'help' => '',
    ),
    'kurikulum_sekolah' => array(
      'name' => t('Kurikulum sekolah'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'osis' => array(
      'name' => t('OSIS'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nama Kegiatan'),
      'help' => '',
    ),
    'prestasi' => array(
      'name' => t('Prestasi'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nama'),
      'help' => '',
    ),
    'slide_show' => array(
      'name' => t('Slide show'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
